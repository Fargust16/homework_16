﻿#define _CRT_SECURE_NO_WARNINGS
#include <iostream>

int main()
{
    const int n = 7;

    int arr[n][n];

    time_t now = time(0);
    tm *ltm = localtime(&now);

    int curDay = ltm -> tm_mday;
    int sum = 0;

    for (size_t i = 0; i < n; i++)
    {
        for (size_t j = 0; j < n; j++)
        {
            arr[i][j] = i + j;
            std::cout << arr[i][j] << '\t';


            if (i == curDay % n) {
                sum += arr[i][j];
            }
        }
        std::cout << '\n';
    }

    std::cout << "\nCurrent day: " << curDay << '\n' << "N: " << n << std::endl;
    std::cout << "\nSum (row index: " << curDay % n << ") = " << sum << std::endl;
}